import React, { Component } from 'react';
import EasyEdit from 'react-easy-edit';
import { Grid } from 'semantic-ui-react';

class EditableArea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editableItems: this.props.editableItems
    };

    this.handleEdit = this.handleEdit.bind(this);
  }

  handleEdit(params, val, index) {
    let statecopy = this.state.editableItems.slice();
    statecopy[index].value = val;
    this.setState({
      editableItems: statecopy
    })
    this.props.onEdit(...params, val)
  }

  render() {
    return (
      <Grid>
        {
          this.state.editableItems.map((editableItem, index) => {
            return (
              <Grid.Column key={index} width={4} className={!editableItem.value ? 'empty' : ''}>
                <label className="d-block bold">{editableItem.label}</label>
                {
                  <EasyEdit
                    type="text"
                    value={editableItem.value ? editableItem.value : 'Empty'}
                    onSave={(val) => this.handleEdit(editableItem.params, val, index)}
                  />
                }
              </Grid.Column>
            );
          })
        }
      </Grid>
    );
  }
}

export default EditableArea;