import React, { Component } from 'react';
import { Form, Header, Container } from 'semantic-ui-react';

class CreateForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      [this.props.modelName]: { ...this.props.model },
    };

    // Bind this context to component's methods.
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(filedName, filedValue) {
    this.setState({
      [this.props.modelName]: {
        ...this.state[this.props.modelName],
        [filedName]: filedValue
      }
    });
  }

  handleSubmit() {
    this.props.onSubmit(this.state[this.props.modelName], this.props.index);
    let modelCopy = { ...this.state[this.props.modelName] }
    for (let modelProp in modelCopy) {
      modelCopy[modelProp] = '';
    }
    this.setState({
      [this.props.modelName]: modelCopy
    });
  }

  render() {
    return (
      <Container fluid>
        <Header as='h4'>{this.props.title}</Header>
        <Form onSubmit={this.handleSubmit}>
          <Form.Group>
            <Form.Input
              required
              placeholder='English Name *'
              name='name'
              value={this.state[[this.props.modelName]].name}
              onChange={(e) => this.handleChange(e.target.name, e.target.value)}
            />
            <Form.TextArea
              rows={2}
              placeholder='English Description'
              name='description'
              value={this.state[this.props.modelName].description}
              onChange={(e) => this.handleChange(e.target.name, e.target.value)}
            />
            <Form.Button color='green' content='Submit' />
          </Form.Group>
        </Form>
      </Container>
    );
  }
}

export default CreateForm;