import React, { Component } from 'react';
import axiosBase from '../../global-axios';
import { withRouter } from 'react-router-dom';

// Style
import './style.css';

// Services
import AuthService from '../../services/AuthService';
import CategoryService from '../../services/CategoryService';
import ItemService from '../../services/ItemService';

// Components
import { Accordion, Icon, Button, Header, Grid } from 'semantic-ui-react';
import EasyEdit from 'react-easy-edit';
import CreateForm from '../../components/CreateForm';
import EditableArea from '../../components/EditableArea';

let Categories, Items;

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userRole: '',
      activeCategory: null,
      menuData: {
        categories: []
      }
    }
    // Bind this context to component's methods.
    this.addCategory = this.addCategory.bind(this);
    this.editCategory = this.editCategory.bind(this);
    this.deleteCategory = this.deleteCategory.bind(this);
    this.addItem = this.addItem.bind(this);
    this.editItem = this.editItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
    this.updateMenuData = this.updateMenuData.bind(this);
    this.logout = this.logout.bind(this);
  }

  // Methods

  /**
   * This method make 2 tasks:
   *  - Get menu data either from local storage or from 'data.json' file.
   *  - Set 'menuData' state.
   */
  async init() {
    let menuData = JSON.parse(localStorage.getItem('menuData'));
    if (!menuData) {
      let response = await axiosBase.get('data.json');
      menuData = response.data;
      Categories = new CategoryService(menuData);
      Items = new ItemService(menuData);
      this.updateMenuData(menuData);
    }
    else {
      Categories = new CategoryService(menuData);
      Items = new ItemService(menuData);
      this.updateMenuData(menuData);
    }
  }

  addCategory(newCategory) {
    const newMenuData = Categories.add(newCategory);
    this.updateMenuData(newMenuData);
  }

  editCategory(categoryIndex, prop, value) {
    const newMenuData = Categories.edit(categoryIndex, prop, value);
    this.updateMenuData(newMenuData);
  }

  deleteCategory(categoryIndex) {
    const newMenuData = Categories.delete(categoryIndex);
    this.updateMenuData(newMenuData);
  }

  addItem(newItem, categoryIndex) {
    const newMenuData = Items.add(newItem, categoryIndex);
    this.updateMenuData(newMenuData);
  }

  editItem(categoryIndex, itemIndex, prop, value) {
    const newMenuData = Items.edit(categoryIndex, itemIndex, prop, value);
    this.updateMenuData(newMenuData);
  }

  deleteItem(categoryIndex, itemIndex) {
    const newMenuData = Items.delete(categoryIndex, itemIndex);
    this.updateMenuData(newMenuData);
  }

  updateMenuData(menuData) {
    this.setState({
      menuData: menuData
    });
    localStorage.setItem('menuData', JSON.stringify(menuData));
  }

  handleAccordionClick(stateName, activeCategory, event) {
    this.state[stateName] !== activeCategory ?
      this.setState({ [stateName]: activeCategory }) :
      this.setState({ [stateName]: null })
  }

  logout() {
    const logOutState = AuthService.logOut();
    logOutState === 'success' ?
      this.props.history.push('/') :
      alert('Logout faild!')
  }

  // LifeCycle Hooks
  componentWillMount() {
    if (!AuthService.userAuthenticated())
      this.props.history.push('/');
    this.setState({
      userRole: AuthService.userRole()
    })
  }
  async componentDidMount() {
    await this.init();
  }

  render() {
    return (
      <div id="wrapper">
        <main>

          {/* Create Category Section For Only Admins. */}

          {
            this.state.userRole === 'admin' &&
            <section className="category-create">
              <CreateForm
                onSubmit={this.addCategory}
                title="Add Category"
                model={{
                  name: '',
                  description: ''
                }}
                modelName="category">
              </CreateForm>
            </section>
          }

          {/* Menu Data Section */}

          <section className="menu-data">
            <Header as='h4'>Menu Data</Header>

            {/* Category List Section */}

            <section className="category-list">
              <Accordion styled>
                {this.state.menuData.categories.map((category, categoryIndex) => {
                  return (

                    /* ===== Single Category ===== */

                    <div className="category" key={category.id}>

                      {/* Category Title */}

                      <Accordion.Title name="categoryAccordion" active={this.state.activeCategory === categoryIndex} index={categoryIndex} onClick={(e) => this.handleAccordionClick('activeCategory', categoryIndex, e)}>
                        <Icon name='dropdown' />
                        {category.name}
                        {
                          this.state.userRole === 'admin' &&
                          <div className="actions">
                            <Button color='red' onClick={() => this.deleteCategory(categoryIndex)}>Delete</Button>
                          </div>
                        }
                      </Accordion.Title>

                      {/* Category Content */}

                      <Accordion.Content active={this.state.activeCategory === categoryIndex}>
                        {
                          this.state.userRole === 'admin' ?

                            /* ===== Edit Category Section Only For Admins ===== */

                            <section className="category-edit">
                              <EditableArea
                                editableItems={[
                                  {
                                    label: 'Name *',
                                    value: category.name,
                                    params: [categoryIndex, 'name']
                                  },
                                  {
                                    label: 'Description',
                                    value: category.description,
                                    params: [categoryIndex, 'description']
                                  }
                                ]}
                                onEdit={this.editCategory}
                              />
                            </section>

                            :

                            /* ===== Category Info Section Only For Users ===== */

                            <section className="category-info">
                              <Grid>
                                <Grid.Column width={4}>
                                  <label className="d-block bold">Name</label>
                                  <p className="bold">{category.name}</p>
                                </Grid.Column>
                                <Grid.Column width={4} className={!category.description ? 'empty' : ''}>
                                  <label className="d-block bold">Description</label>
                                  <p className="bold">{category.description ? category.description : 'Empty'}</p>
                                </Grid.Column>
                              </Grid>
                            </section>

                        }

                        {/* Create Item Section Only For Admins */}

                        {
                          this.state.userRole === 'admin' &&
                          <section className="item-create">
                            <CreateForm
                              onSubmit={this.addItem}
                              title="Add Item"
                              index={categoryIndex}
                              model={{
                                name: '',
                                description: '',
                                price: ''
                              }}
                              modelName="item">
                            </CreateForm>
                          </section>
                        }

                        {/* Item List Section */}

                        <section className="item-list">
                          {category.items.map((item, itemIndex) => {
                            return (

                              /* ===== Single Item Section ===== */

                              <div className="category__item" key={item.id}>
                                <Accordion styled>

                                  {/* Item Title */}

                                  <Accordion.Title name="itemAccordion" active={this.state[`activeItem${categoryIndex}`] === itemIndex} index={itemIndex} onClick={() => this.handleAccordionClick(`activeItem${categoryIndex}`, itemIndex)}>
                                    <Icon name='dropdown' />
                                    {item.name}
                                    {
                                      this.state.userRole === 'admin' &&
                                      <div className="actions">
                                        <Button color='red' onClick={() => this.deleteItem(categoryIndex, itemIndex)}>Delete</Button>
                                      </div>
                                    }
                                  </Accordion.Title>

                                  {/* Item Content */}

                                  <Accordion.Content active={this.state[`activeItem${categoryIndex}`] === itemIndex}>
                                    {
                                      this.state.userRole === 'admin' ?

                                        /* ===== Item Edit Section Only For Admins ===== */

                                        <section className="item-edit">
                                          <EditableArea
                                            editableItems={[
                                              {
                                                label: 'Name *',
                                                value: item.name,
                                                params: [categoryIndex, itemIndex, 'name']
                                              },
                                              {
                                                label: 'Description',
                                                value: item.description,
                                                params: [categoryIndex, itemIndex, 'description']
                                              },
                                              {
                                                label: 'Price',
                                                value: item.price,
                                                params: [categoryIndex, itemIndex, 'price']
                                              }
                                            ]}
                                            onEdit={this.editItem}
                                          />
                                        </section>

                                        :

                                        /* ===== Item Info Section Only For Users ===== */

                                        <section className="item-info">
                                          <Grid>
                                            <Grid.Column width={4}>
                                              <label className="d-block bold">Name</label>
                                              <p className="bold">{item.name}</p>
                                            </Grid.Column>
                                            <Grid.Column width={4} className={!item.description ? 'empty' : ''}>
                                              <label className="d-block bold">Description</label>
                                              <p className="bold">{item.description ? item.description : 'Empty'}</p>
                                            </Grid.Column>
                                            <Grid.Column width={4} className={!item.price ? 'empty' : ''}>
                                              <label className="d-block bold">Price</label>
                                              <p className="bold">{item.price ? item.price : 'Empty'}</p>
                                            </Grid.Column>
                                          </Grid>
                                        </section>

                                    }
                                  </Accordion.Content>
                                </Accordion>
                              </div>
                            )
                          })}
                        </section>

                      </Accordion.Content>
                    </div>
                  );
                })}
              </Accordion>
            </section>
          </section>
        </main>
        <section className="logout">
          <Button primary onClick={() => this.logout()}>Logout</Button>
        </section>
      </div>
    );
  }
}

export default withRouter(Menu);