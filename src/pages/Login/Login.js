import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';


// Services
import AuthService from '../../services/AuthService';

// Components
import { Form, Input, Button, Container } from 'semantic-ui-react';

// Style
import './style.css';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: ''
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  // Methods
  handleSubmit() {
    const loginState = AuthService.login(this.state.username, this.state.password);
    loginState === 'success' ?
      this.props.history.push('/menu') :
      this.props.history.push('/')

  }

  handleChange(filedName, filedValue) {
    this.setState({
      [filedName]: filedValue
    });
  }

  // LifeCycle Hooks
  componentWillMount() {
    if (AuthService.userAuthenticated())
      this.props.history.push('/menu');
  }

  render() {
    return (
      <main className="main-content">
        <section className="login-form">
          <Container fluid>
            <Form onSubmit={this.handleSubmit}>
              <Form.Field>
                <label>User Name</label>
                <Input
                  name="username"
                  placeholder='User Name'
                  onChange={(e) => this.handleChange(e.target.name, e.target.value)}
                  required
                />
              </Form.Field>
              <Form.Field>
                <label>Password</label>
                <Input
                  type="password"
                  name="password"
                  placeholder='User Name'
                  onChange={(e) => this.handleChange(e.target.name, e.target.value)}
                  required
                />
              </Form.Field>
              <Button type='submit'>Submit</Button>
            </Form>
          </Container>
        </section>
      </main >
    )
  }
}

export default withRouter(Login);