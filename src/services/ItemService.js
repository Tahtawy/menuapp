class ItemService {
  constructor(menuData) {
    this.menuData = menuData;
  }

  add(newItem, categoryIndex) {
    newItem.id = Math.floor(Math.random() * 10000);
    this.menuData.categories[categoryIndex].items.push(newItem);
    return this.menuData;
  }

  edit(categoryIndex, itemIndex, prop, value) {
    this.menuData.categories[categoryIndex].items[itemIndex][prop] = value;
    return this.menuData;
  }

  delete(categoryIndex, itemIndex) {
    this.menuData.categories[categoryIndex].items.splice(itemIndex, 1);
    return this.menuData;
  }
}

export default ItemService;