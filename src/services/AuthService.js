class AuthService {
  static login(userName, password) {
    try {
      if (userName === 'admin' && password === 'admin')
        localStorage.setItem('user', JSON.stringify({
          logined: true,
          role: 'admin'
        }));
      else
        localStorage.setItem('user', JSON.stringify({
          logined: true,
          role: 'user'
        }));
      return 'success';
    } catch (error) {
      console.error(error);
      return 'error'
    }
  }

  static userAuthenticated() {
    const isUserAuthenticated = JSON.parse(localStorage.getItem('user'));
    if (!isUserAuthenticated)
      return false;
    return true;
  }

  static userRole() {
    const user = JSON.parse(localStorage.getItem('user'));
    if (!user)
      return false;
    return user.role;
  }

  static logOut() {
    try {
      localStorage.removeItem('user')
      return 'success';
    } catch (error) {
      console.error(error);
      return 'error'
    }
  }
}

export default AuthService;