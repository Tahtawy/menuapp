class CategoryService {
  constructor(menuData) {
    this.menuData = menuData;
  }

  add(newCategory) {
    newCategory.id = Math.floor(Math.random() * 10000);
    newCategory.items = [];
    this.menuData.categories.push(newCategory);
    return this.menuData;
  }

  edit(categoryIndex, prop, value) {
    this.menuData.categories[categoryIndex][prop] = value;
    return this.menuData;
  }

  delete(categoryIndex) {
    this.menuData.categories.splice(categoryIndex, 1);
    return this.menuData;
  }
}

export default CategoryService;