import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import createRoutes from './routes';

// Semantic UI Default Theme
import 'semantic-ui-css/semantic.min.css';

const routes = createRoutes(); // Haaaay our routes here :D

ReactDOM.render(
  routes,
  document.getElementById('root')
);
