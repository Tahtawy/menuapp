import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

// Pages
import Login from './pages/Login/Login';
import Menu from './pages/Menu/Menu';

const createRoutes = () => (
  <Router>
    <Route path="/" exact component={Login} />
    <Route path="/menu" exact component={Menu} />
  </Router>
);

export default createRoutes;